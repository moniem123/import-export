@extends('admin.layouts.layout')
@section('page-title')
    المسلم - {{$user->name}}
@endsection
@section('content')
    <div class="page-header page-header-light ">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline ">
            <div class="d-flex mr-auto">
                <div class="breadcrumb">
                    <a href="{{route('admin.home')}}" class="breadcrumb-item">
                        <i class="icon-home2 mr-2"></i>
                       الرئيسية
                    </a>
                    <a href="{{route('admin.users.index')}}" class="breadcrumb-item">المسلمين</a>
                    <span class="breadcrumb-item active">@yield('page-title')</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>

    <!-- Content area -->
    <div class="content">
        <!-- Basic initialization -->
        <div class="panel panel-flat">
            @include('admin.layouts.status')

            <div class="panel-body mb-2">
                <a href="{{route('admin.users.create')}}" class="btn btn-primary mr-3">
                    <i class="icon-add"  style="margin-left: 10px;"></i>
                    اضافة مسلم
                </a>
            </div>

            <table class="table datatable-button-init-basic table-hover table-responsive display nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>البريد الاليكترونى</th>
                    <th>رقم الهاتف</th>
                    <th>الداعية</th>
                    <th>الدولة</th>
                    <th>الجنسية</th>
                    <th>الديانة</th>
                    <th>صفحة الفبس بوك</th>
                    <th>كيف تعرفت علينا</th>
                    <th>تاريخ الاسلام</th>
                    <th>اللغة</th>
                    <th class="text-center">الصوره</th>
                    <th class="text-center"> وثيقة الاسلام </th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>#</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone ?? 'لا يوجد'}}</td>
                        <td>{{optional($user->lecturer)->name ?? 'لا يوجد'}}</td>
                        <td>{{optional($user->country)->name() ?? 'لا يوجد'}}</td>
                        <td>{{optional($user->nationality)->name() ?? 'لا يوجد'}}</td>
                        <td>{{$user->religion ?? 'لا يوجد'}}</td>
                        <td>{{$user->facebook_page ?? 'لا يوجد'}}</td>
                        <td>{{$user->how_know_us ?? 'لا يوجد'}}</td>
                        <td>{{date('Y-m-d',strtotime($user->islam_date)) ?? 'لا يوجد'}}</td>
                        <td>{{$user->lang ?? 'لا يوجد'}}</td>
                        <td class="text-center">
                            @if($user->image)
                                <img src="{{$user->image}}" class="img-thumbnail" alt="user_img" width="70" height="70">
                            @else
                                <p>لا توجد</p>
                            @endif
                        </td>
                        <td class="text-center">
                            @if($user->islam_certificate)
                                <img src="{{$user->islam_certificate}}" class="img-thumbnail" alt="user_img" width="70" height="70">
                            @else
                                <p>لا توجد</p>
                            @endif
                        </td>

                </tbody>
            </table>
        </div>
        <!-- /basic initialization -->
    </div>
    <!-- /content area -->
@endsection
