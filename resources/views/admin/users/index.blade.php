@extends('admin.layouts.layout')
@section('page-title')
    المسلمين
@endsection
@section('content')
    <div class="page-header page-header-light ">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline ">
            <div class="d-flex mr-auto">
                <div class="breadcrumb">
                    <a href="{{route('admin.home')}}" class="breadcrumb-item">
                        <i class="icon-home2 mr-2"></i>
                       الرئيسية
                    </a>
                    <span class="breadcrumb-item active">@yield('page-title')</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>

    <!-- Content area -->
    <div class="content">
        <!-- Basic initialization -->
        <div class="panel panel-flat">
            @include('admin.layouts.status')

            <div class="panel-body mb-2">
                <a href="{{route('admin.users.create')}}" class="btn btn-primary mr-3">
                    <i class="icon-add"  style="margin-left: 10px;"></i>
                    اضافة مسلم
                </a>
            </div>

            <table class="table datatable-button-init-basic table-hover table-responsive display nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>البريد الاليكترونى</th>
                    <th>رقم الهاتف</th>
                    <th>السن</th>
                    <th>النوع</th>
                    <th>الداعية</th>
                    <th class="text-center">الصوره</th>
                    <th class="text-center">العمليات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $index => $user)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone ?? 'لا يوجد'}}</td>
                        <td>{{$user->age ?? 'لا يوجد'}}</td>
                        <td>{{$user->gender ?? 'لا يوجد'}}</td>
                        <td>{{optional($user->lecturer)->name ?? 'لا يوجد'}}</td>
                        <td class="text-center">
                            @if($user->image)
                                <img src="{{$user->image}}" class="img-thumbnail" alt="user_img" width="70" height="70">
                            @else
                                <p>لا توجد</p>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group text-center">
                                @if($user->is_active == 0)
                                    <button disabled="disabled" class="btn btn-sm btn-warning disabled">غير مفعل</button>
                                @else
                                    <button disabled class="btn btn-sm btn-success">مفعل</button>
                                @endif

                                <div class=" text-center">
                                    <a href="{{route('admin.users.show',$user->id)}}"
                                       class="btn btn-info btn-sm ml-2 rounded-circle">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>

                                <div class=" text-center">
                                    <a href="{{route('admin.users.edit',$user->id)}}"
                                       class="btn btn-primary btn-sm ml-2 rounded-circle">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>

                                {!! Form::open(['route' => ['admin.users.destroy',$user->id],'method' => 'delete']) !!}
                                <button class="btn btn-danger btn-sm ml-2 rounded-circle">
                                    <i class="fa fa-trash"></i>
                                </button>
                                {!! Form::close() !!}

                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /basic initialization -->
    </div>
    <!-- /content area -->
@endsection
