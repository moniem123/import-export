<div class="form-group row">
    <label for="name" class="col-form-label col-12 col-lg-2">الاسم</label>
    <div class="col-12 col-lg-4">
        {!! Form::text('name',null,['class' =>'form-control '.($errors->has('name') ? ' is-invalid' : null),'placeholder'=> 'الاسم' ,]) !!}
        @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="email" class="col-form-label col-12 col-lg-2 text-lg-right"> البريد الاليكترونى</label>
    <div class="col-12 col-lg-4">
        {!! Form::email('email',null,['class' =>'form-control '.($errors->has('email') ? ' is-invalid' : null),'placeholder'=> ' البريد الاليكترونى' ,]) !!}
        @error('email')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="phone" class="col-form-label col-12 col-lg-2"> رقم الهاتف </label>
    <div class="col-12 col-lg-4">
        {!! Form::tel('phone',null,['class' =>'form-control '.($errors->has('phone') ? ' is-invalid' : null),'placeholder'=> ' رقم الهاتف ' ,]) !!}
        @error('phone')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="country_id" class="col-form-label col-12 col-lg-2 text-lg-right">الدولة </label>
    <div class="col-12 col-lg-4">

        {!! Form::select("country_id",country(),null,
        ['class'=>"form-control ".($errors->has('country_id')?'is-invalid' : null),'data-show-subtext'=>'true','data-live-search'=>'true','id' => 'country_id','placeholder'=>'اختر الدولة '])!!}

        @error('country_id')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

</div>

<div class="form-group row">
    <label for="gender" class="col-form-label col-12 col-lg-2">النوع</label>
    <div class="col-12 col-lg-4">
        {!! Form::select("gender",gender('ar'),null,
        ['class'=>"form-control ".($errors->has('gender')?'is-invalid' : null),'data-show-subtext'=>'true','data-live-search'=>'true','id' => 'gender','placeholder'=>'اختر النوع '])!!}

        @error('gender')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="nationality_id" class="col-form-label col-12 col-lg-2 text-lg-right">الجنسية </label>
    <div class="col-12 col-lg-4">

        {!! Form::select("nationality_id",nationality(),null,
        ['class'=>"form-control ".($errors->has('nationality_id')?'is-invalid' : null),'data-show-subtext'=>'true','data-live-search'=>'true','id' => 'nationality_id','placeholder'=>'اختر الجنسية '])!!}

        @error('nationality_id')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

</div>

<div class="form-group row">
    <label for="lecturer_id" class="col-form-label col-12 col-lg-2">الداعية</label>
    <div class="col-12 col-lg-4">
        {!! Form::select("lecturer_id",lecturer(),null,
        ['class'=>"form-control ".($errors->has('lecturer_id')?'is-invalid' : null),'data-show-subtext'=>'true','data-live-search'=>'true','id' => 'lecturer_id','placeholder'=>'اختر الداعية '])!!}

        @error('lecturer_id')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="religion" class="col-form-label col-12 col-lg-2 text-lg-right">الديانة </label>
    <div class="col-12 col-lg-4">
        {!! Form::select("religion",religion('ar'),null,
        ['class'=>"form-control ".($errors->has('religion')?'is-invalid' : null),'data-show-subtext'=>'true','data-live-search'=>'true','id' => 'religion','placeholder'=>'اختر الديانة '])!!}

        @error('religion')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>


</div>

<div class="form-group row">
    <label for="password" class="col-form-label col-12 col-lg-2">  كلمة المرور </label>
    <div class="col-12 col-lg-4">
        {!! Form::password('password',['class' =>'form-control '.($errors->has('password') ? ' is-invalid' : null),'id' => 'password','placeholder'=> ' كلمة المرور ' ,]) !!}
        @error('password')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="password_confirmation" class="col-form-label col-12 col-lg-2 text-lg-right">تاكيد كلمة المرور</label>
    <div class="col-12 col-lg-4">
        {!! Form::password('password_confirmation' ,['class' =>'form-control '.($errors->has('password_confirmation') ? ' is-invalid' : null),'id' => 'password_confirmation','placeholder'=> 'تاكيد كلمة المرور' ,]) !!}
        @error('password_confirmation')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="lang" class="col-form-label col-12 col-lg-2">اللغة</label>
    <div class="col-12 col-lg-4">
        {!! Form::text("lang[]",null,
        ['class'=>"form-control ".($errors->has('lang[]')?'is-invalid' : null),'data-show-subtext'=>'true','data-live-search'=>'true','id' => 'lang','placeholder'=>'اللغة '])!!}

        @error('lang[]')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="facebook_page" class="col-form-label col-12 col-lg-2 text-lg-right">صفحة الفيس بوك</label>
    <div class="col-12 col-lg-4">
        {!! Form::text('facebook_page',null,['class' =>'form-control '.($errors->has('facebook_page') ? ' is-invalid' : null),'id'=>'facebook_page','placeholder'=> 'صفحة الفيس بوك' ,]) !!}
        @error('facebook_page')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="how_know_us" class="col-form-label col-12 col-lg-2">كيف تعرفت  علينا</label>
    <div class="col-12 col-lg-4">
        {!! Form::text('how_know_us',null,['class' =>'form-control '.($errors->has('how_know_us') ? ' is-invalid' : null),'id'=>'how_know_us','placeholder'=> 'كيف تعرفت  علينا' ,]) !!}
        @error('how_know_us')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="islam_date" class="col-form-label col-12 col-lg-2 text-lg-right">تاريخ الاسلام</label>
    <div class="col-12 col-lg-4">
        {!! Form::date('islam_date',null,['class' =>'form-control '.($errors->has('islam_date') ? ' is-invalid' : null),'id'=>'islam_date','placeholder'=> 'تاريخ الاسلام' ,]) !!}
        @error('islam_date')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="age" class="col-form-label col-12 col-lg-2">السن</label>
    <div class="col-12 col-lg-4">
        {!! Form::text('age',null,['class' =>'form-control '.($errors->has('age') ? ' is-invalid' : null),'id'=>'age','placeholder'=> 'السن' ,]) !!}
        @error('age')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

</div>

<div class="form-group row">
    <label for="image" class="col-form-label col-12 col-lg-2"> الصوره </label>
    <div class="col-12 col-lg-4">
        {!! Form::file('image',['class'=>'form-control'.($errors->has('image') ? ' is-invalid' : null),'id'=>'image']) !!}
        @error('image')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    @if(isset($user))
        <label for="image" class="col-form-label col-12 col-lg-2 text-lg-right">الصوره</label>
        <div class="col-12 col-lg-4">
            @if($user->image)
                <img src="{{$user->image}}" class="img-thumbnail" alt="user_img" width="100" height="80" id="image">
            @else {{__('No Image')}} @endif
        </div>
    @endif

</div>
<div class="form-group row">
    <label for="islam_certificate" class="col-form-label col-12 col-lg-2"> وثيقة الاسلام </label>
    <div class="col-12 col-lg-4">
        {!! Form::file('islam_certificate',['class'=>'form-control'.($errors->has('islam_certificate') ? ' is-invalid' : null)]) !!}
        @error('islam_certificate')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    @if(isset($user))
        <label for="islam_certificate" class="col-form-label col-12 col-lg-2 text-lg-right">وثيقة الاسلام</label>
        <div class="col-12 col-lg-4">
            @if($user->islam_certificate)
                <img src="{{$user->islam_certificate}}" class="img-thumbnail" alt="user_img" width="100" height="80" id="islam_certificate">
            @else {{__('No Image')}} @endif
        </div>
    @endif


</div>

<div class="form-group row">
    <button type="submit" class="btn btn-primary btn-block">حفظ</button>
</div>
