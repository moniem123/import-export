@extends('admin.layouts.layout')
@section('page-title')
    اضافة مسلم
@endsection
@section('content')
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex mr-auto">
                <div class="breadcrumb">
                    <a href="{{route('admin.home')}}" class="breadcrumb-item">
                        <i class="icon-home2 mr-2"></i>
                        الرئيسية
                    </a>
                    <a href="{{route('admin.users.index')}}" class="breadcrumb-item">المسلمين</a>
                    <span class="breadcrumb-item active">@yield('page-title')</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- Content area -->
    <div class="content">
        <!-- Form horizontal -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h3 class="panel-title"> اضافة مسلم </h3>
            </div>
            <hr>
            <div class="panel-body">
                {!! Form::open(['route' => 'admin.users.store','method'=>'post','files' =>true,]) !!}
                    @include('admin.users.form')
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /form horizontal -->
    </div>
    <!-- /content area -->

@endsection

{{--@section('my-js')--}}
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            $('#region_id').on('change', function (e) {--}}
{{--                var region_id = $(this).val();--}}
{{--                if (region_id) {--}}
{{--                    $.ajax({--}}
{{--                        url: '/dashboard/getCities/' + region_id,--}}
{{--                        method: 'GET',--}}
{{--                        type: 'json',--}}
{{--                        success: function (data) {--}}
{{--                            //console.log(data);--}}
{{--                            $('#city_id').empty();--}}
{{--                            $.each(data, function (key, value) {--}}
{{--                                $('#city_id').append('<option value="' + key + '" >' + value + '</option>');--}}
{{--                            });--}}
{{--                        }--}}
{{--                    });--}}
{{--                } else {--}}
{{--                    $('#city_id').empty();--}}
{{--                }--}}
{{--            });--}}
{{--            ////////////////--}}
{{--            $('#city_id').on('click', function (e) {--}}
{{--                var city_id = $(this).val();--}}
{{--                if (city_id) {--}}
{{--                    $.ajax({--}}
{{--                        url: '/dashboard/getDistricts/' + city_id,--}}
{{--                        method: 'GET',--}}
{{--                        type: 'json',--}}
{{--                        success: function (data) {--}}
{{--                            //console.log(data);--}}
{{--                            $('#district_id').empty();--}}
{{--                            $.each(data, function (key, value) {--}}
{{--                                $('#district_id').append('<option value="' + key + '" >' + value + '</option>');--}}
{{--                            });--}}
{{--                        }--}}
{{--                    });--}}
{{--                } else {--}}
{{--                    $('#district_id').empty();--}}
{{--                }--}}
{{--            });--}}
{{--            /////////////////////--}}
{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}


