@extends('dashboard.layouts.layout')
@section('page-title')
    {{__('Roles')}}
@endsection
@section('content')
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex mr-auto">
                <div class="breadcrumb">
                    <a href="{{route('admin.main')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        {{__('Main')}}</a>
                    <span class="breadcrumb-item active">@yield('page-title')</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- Content area -->
    <div class="content">
        <!-- Basic initialization -->
        <div class="panel panel-flat">
            @include('dashboard.layouts.status')

            <div class="panel-body mb-2">
                <a href="{{route('admin.roles.create')}}" class="btn btn-primary mr-3"><i class="icon-add"
                                                                                          style="margin-left: 10px;"></i>
                    {{__('Add Role')}}</a>
            </div>

            <table class="table datatable-button-init-basic table-hover table-responsive display nowrap"
                   style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th> {{__('Show')}}</th>
                    <th class="text-center">{{__('Operations')}}</th>

                </tr>
                </thead>
                <tbody>
                @foreach($roles as $index => $role)
                    <tr>
                        <td>{{$index +1}}</td>
                        <td>{{$role->name}}</td>
                        <td>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#item{{$role->id}}">
                                {{__('View Permissions')}}
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="item{{$role->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="item{{$role->id}}" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-danger-300">
                                            <h5 class="modal-title"
                                                id="exampleModalScrollableTitle">{{__('Permissions List')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body p-4">
                                            <div class="row">
                                                @if(count($role->permissions))
                                                    @foreach($role->permissions as $permission)
                                                        <div class="col-sm-3">
                                                            <ul>
                                                                @if(app()->getLocale() == 'ar')
                                                                    <li>{{$permission->ar_name}}</li>
                                                                @else
                                                                    <li>{{$permission->name}}</li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    {{__('No Permissions')}}
                                                @endif
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            @if(!$role->hasAllPermissions(\Spatie\Permission\Models\Permission::all()))
                                                <a href="{{route('admin.roles.edit',$role->id)}}"
                                                   class="btn btn-warning">{{__('Edit')}}</a>
                                            @endif
                                            <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">{{__('Close')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>

                        <td class="text-center">
                            <div class="btn-group text-center">

                                @if(!$role->hasAllPermissions(\Spatie\Permission\Models\Permission::all()))
                                    @if( !auth()->user()->hasRole($role))
                                        <form action="{{ route('admin.active.role', ['id' => $role->id]) }}"
                                              method="post">@csrf
                                            <button type="submit"
                                                    class="{{ $role->is_active ? 'btn btn-success' : 'btn btn-warning' }}">{{ $role->is_active ? __('Active') : __('Deactivate') }}</button>
                                        </form>
                                    @else
                                        <button disabled class="btn btn-success">{{__('Active')}}</button>
                                    @endif
                                @else
                                    <button disabled class="btn btn-success">{{__('Active')}}</button>
                                @endif


                                @if(!$role->hasAllPermissions(\Spatie\Permission\Models\Permission::all()))
                                    @if( !auth()->user()->hasRole($role))
                                        {!! Form::open([
                                                        'action' => ['Admin\RoleController@destroy',$role->id],
                                                        'method' => 'delete'
                                                        ]) !!}
                                        <button class="btn btn-danger btn-sm ml-2 rounded-circle"><i
                                                class="fa fa-trash"></i></button>
                                        {!! Form::close() !!}
                                    @else
                                        <button class="btn btn-danger btn-sm ml-2 rounded-circle" disabled><i
                                                class="fa fa-trash"></i>
                                        </button>
                                    @endif
                                @else
                                    <button class="btn btn-danger btn-sm ml-2 rounded-circle" disabled><i
                                            class="fa fa-trash"></i>
                                    </button>
                                @endif

                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /basic initialization -->
    </div>
    <!-- /content area -->
@endsection

