@extends('dashboard.layouts.layout')
@section('page-title')
    {{__('Add Role')}}
@endsection
@inject('model','Spatie\Permission\Models\Role')
@section('content')
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex mr-auto">
                <div class="breadcrumb">
                    <a href="{{route('admin.main')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        {{__('Main')}}</a>
                    <a href="{{route('admin.roles.index')}}" class="breadcrumb-item">{{__('Roles')}}</a>
                    <span class="breadcrumb-item active">@yield('page-title')</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- Content area -->
    <div class="content">
        <!-- Form horizontal -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h3 class="panel-title">{{__('Add Role')}}</h3>
            </div>
            <hr>
            <div class="panel-body">
                {!! Form::model($model,[
                'action' => 'Admin\RoleController@store',
                'files' =>true
                ]) !!}
                <div class="form-group row">
                    <label for="name" class="col-form-label col-lg-2 font-weight-bold">{{__('Name')}}</label>
                    <div class="col-lg-10">
                        <input type="text" name="name"
                               class="form-control {{$errors->has('name') ? ' is-invalid' : null}}"
                               value="{{old('name')}}" placeholder="{{__('Name')}}">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group ">
                    <h6 class="font-weight-bold">{{__('Select Permissions')}}</h6>

                    <div class="row mb-2">
                        <div class="col-12">
                            <input type="checkbox" id="select-all" class="mr-2">
                            <label for="select-all">{{__('Select All')}}</label>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($permission as $value)
                            <div class="col-sm-3">
                                <label>
                                    {{ Form::checkbox('permission[]', $value->name, false, array('class' => 'name mr-1')) }}
                                    @if(app()->getLocale() == 'ar')
                                    {{ $value->ar_name }}
                                        @else
                                        {{ $value->name }}
                                        @endif
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group row">
                    <button type="submit" class="btn btn-primary btn-block">{{__('Add')}}</button>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
        <!-- /form horizontal -->
    </div>
    <!-- /content area -->
@endsection

