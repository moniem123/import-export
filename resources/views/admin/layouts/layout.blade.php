<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="auth-user" content="{{auth()->user()}}">
    <title>@yield('page-title')</title>
    <link rel="icon" href="{{asset('dashboard/global_assets/images/logo.png')}}">
    <!---- start generated favicon -->
    <!-- <link rel="apple-touch-icon" sizes="57x57" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('dashboard/global_assets/images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('dashboard/global_assets/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('dashboard/global_assets/images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('dashboard/global_assets/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('dashboard/global_assets/images/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff"> -->
    <!---- end generated favicon -->

    @include('admin.layouts.styles')
    @include('admin.layouts.scripts')
    @yield('styles')
</head>

<body>
    <div id="app">
        @include('admin.layouts.navbar')

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('admin.layouts.sidebar')
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">
                <!-- Content area -->
            @yield('content')
            <!-- /content area -->

                <!-- Footer -->
                <div class="navbar navbar-expand-lg navbar-light">
                    <div class="text-center d-lg-none w-100">
                        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                                data-target="#navbar-footer">
                            <i class="icon-unfold mr-2"></i>
                            Designed By <a href="http://panorama-q.com" target="_blank">Panorama Alqassim</a>
                        </button>
                    </div>
                </div>
                <!-- /footer -->
            </div>
            <!-- /main content -->
        </div>
        <!-- /page content -->
    </div>
<script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.0/dist/JsBarcode.all.min.js"></script>
<script src="{{asset('js/app.js')}}"></script>

@yield('my-js')

@yield('script')

</body>
</html>
