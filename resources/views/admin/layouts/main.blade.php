@extends('admin.layouts.layout')
@section('page-title')
    {{__('Dashboard')}} | {{__('Main')}}
@endsection

@section('content')

    <div class="content">
        <div class="row">


            {{--@can('Users')--}}
                <div class="col-12 col-md-3">
                    <a href="#">
                        <div class="card shadow shadow-md bg-info p-1">
                            <i class="icon-users4 icon-2x"></i>
                            <div class="card-body text-center m-1 font-weight-bold">
                                المستخدمين
                            </div>
                            <label class="badge badge-dark" style="font-size: 14px;">150</label>
                        </div>
                    </a>
                </div>
            {{--@endcan--}}


            {{--@can('HaragBanners')--}}
                <div class="col-12 col-md-3">
                    <a href="#">
                        <div class="card shadow shadow-md bg-primary-600">
                            <i class="icon-image-compare icon-2x"></i>
                            <div class="card-body text-center m-1 font-weight-bold">
                               الوارد
                            </div>
                            <label class="badge badge-dark" style="font-size: 14px;">350</label>
                        </div>
                    </a>
                </div>
            {{--@endcan--}}


{{--            @can('DaleelDarbk')--}}
                <div class="col-12 col-md-3">
                    <a href="#">
                        <div class="card shadow shadow-md bg-slate-400">
                            <i class="icon-list3 icon-2x"></i>
                            <div class="card-body text-center m-1 font-weight-bold">
                               الصادر
                            </div>
                            <label class="badge badge-dark" style="font-size: 14px;">850</label>
                        </div>
                    </a>
                </div>
            {{--@endcan--}}




        </div>
    </div>
@endsection
