<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Cairo:300,400,600,700&display=swap&subset=arabic" rel="stylesheet">
<link href="{{asset('dashboard/global_assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dashboard/global_assets/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">

<link href="{{asset('dashboard/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dashboard/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dashboard/css/layout.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dashboard/css/components.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dashboard/css/colors.css')}}" rel="stylesheet" type="text/css">

<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css
" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css
" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css
" rel="stylesheet" type="text/css">


<link href="{{asset('dashboard/css/customize.css')}}" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<link href="{{asset('dashboard/global_assets/select2.min.css')}}" rel="stylesheet" type="text/css">

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<style>
    body, html {
        font-family: 'Cairo', sans-serif;
        /*font-size: 16px;*/
    }
    .select2-container--classic .select2-selection--multiple .select2-selection__choice{
        background-color: #404040!important;
    }
    .bg-slate-800 {
        background-color: #89DB83;
    }

    .mx-6 {
        margin-left: 10rem !important;
        margin-right: 10rem !important;
    }

</style>





