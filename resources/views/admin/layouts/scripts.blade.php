
<!-- Core JS files -->
<script src="{{asset('dashboard/global_assets/js/main/jquery.min.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
<!-- /core JS files -->

<script src="{{asset('dashboard/global_assets/js/plugins/loaders/pace.min.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/main/jquery.min.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/core/libraries/bootstrap.min.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>

<!-- Theme JS files -->
<script src="{{asset('dashboard/global_assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
{{--<script src="{{asset('dashboard/global_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>--}}
<script src="{{asset('dashboard/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>

<script src="{{asset('dashboard/js/app.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/demo_pages/dashboard.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js
"></script>
<script src="{{asset('dashboard/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/plugins/notifications/sweet_alert.min.js')}}"></script>

<script>
    $(document).ready(function() {
        var table = $('.table').DataTable( {
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true
        } );
    } );
</script>
<!-- /theme JS files -->

<script src="{{asset('dashboard/global_assets/js/demo_pages/form_multiselect.js')}}"></script>
<script src="{{asset('dashboard/global_assets/select2.min.css')}}"></script>

<script src="{{asset('dashboard/global_assets/js/demo_pages/form_checkboxes_radios.js')}}"></script>



<script src="{{asset('dashboard/global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
{{--<script src="{{asset('dashboard/global_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>--}}
<script src="{{asset('dashboard/global_assets/js/plugins/forms/styling/switch.min.js')}}"></script>

<script src="{{asset('dashboard/global_assets/js/demo_pages/form_validation.js')}}"></script>
<script src="{{asset('dashboard/global_assets/js/plugins/forms/validation/validate.min.js')}}"></script>

<script src="{{asset('dashboard/js/ckeditor.js')}}"></script>


