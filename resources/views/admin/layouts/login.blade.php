<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> الصادر و الوارد و الأرشيف - تسجيل دخول لوحة التحكم </title>
  <link rel="icon" href="{{asset('dashboard/global_assets/images/logo.png')}}">
    <!---- start generated favicon -->
    <!-- <link rel="apple-touch-icon" sizes="57x57" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('dashboard/global_assets/images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('dashboard/global_assets/images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('dashboard/global_assets/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('dashboard/global_assets/images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('dashboard/global_assets/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('dashboard/global_assets/images/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff"> -->
    <!---- end generated favicon -->
    @include('admin.layouts.styles')
    @include('admin.layouts.scripts')

</head>

<body class="bg-slate-800">
<!-- Page content -->
<div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">

            <!-- Login card -->
            <form class="login-form" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="card mb-0 shadow shadow-lg">
                    @if($errors->count()>0)
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>{{$error}}!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endforeach
                    @endif
                    <div class="card-body">
                        <div class="text-center mb-3">
                            {{--<img src="/dashboard/global_assets/images/logo.png" alt="">--}}
                            <h5 class="mb-0 mt-1"> الصادر و الوارد | تسجيل دخول </h5>
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                   oninvalid="this.setCustomValidity('{{__('Email Required')}}')"
                                   onchange="this.setCustomValidity('')"
                                   placeholder="البريد الاليكترونى" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            <div class="form-control-feedback">
                                <i class="icon-envelop text-muted"></i>
                            </div>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-left">
                            <input type="password" placeholder="كلمة المرور" name="password"
                                   oninvalid="this.setCustomValidity('{{__('Password Required')}}')"
                                   onchange="this.setCustomValidity('')"
                                   class="form-control @error('password') is-invalid @enderror" required autocomplete="current-password">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">
                                <i class="icon-circle-right2 mr-2"></i> تسجيل الدخول
                            </button>
                        </div>
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                نسيت كلمة المرور
                            </a>
                        @endif
                    </div>

                </div>

            </form>

            <!-- /login card -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

</body>
</html>
