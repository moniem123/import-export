<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-right8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->

    <!-- Sidebar content -->
    <div class="sidebar-content">


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <router-link to="/home" class="nav-link ">
                        <i class="icon-home4"></i>
                        <span>الرئيسية</span>
                    </router-link>
                </li>

                {{--//////////////////////////////////////////////////////////////////////--}}
                    @if(auth()->user()->role != 'user')
                        <li class="nav-item">
                            <router-link to="/users" class="nav-link">
                                <i class="icon-users4"></i>
                                <span>المستخدمين</span>
                            </router-link>
                        </li>
                    @endif
                    <li class="nav-item ">
                        <router-link to="/import" class="nav-link ">
                            <i class="icon-book"></i>
                            <span>الوارد</span>
                        </router-link>
                    </li>

                    <li class="nav-item">
                        <router-link to="/export" class="nav-link ">
                            <i class="icon-list3"></i>
                            <span>الصادر</span>
                        </router-link>
                    </li>


            </ul>
        </div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->
</div>

<!-- /main sidebar -->
