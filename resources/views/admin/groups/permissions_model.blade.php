<!-- Modal -->
<div class="modal fade" id="item{{$group->id}}" tabindex="-1" role="dialog"
     aria-labelledby="item{{$group->id}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger-300">
                <h5 class="modal-title"
                    id="exampleModalScrollableTitle">الصلاحيات</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body p-4">
                <div class="row">
                    @if(count($group->roles))
                        @foreach($group->roles as $role)
                            <div class="col-sm-3">
                                <ul>
                                    <li>{{$role->ar_name}}</li>
                                </ul>
                            </div>
                        @endforeach
                    @else
                        لا توجد صلاحيات
                    @endif
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    اغلاق
                </button>
            </div>
        </div>
    </div>
</div>
