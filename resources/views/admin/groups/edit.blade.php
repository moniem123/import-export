@extends('admin.layouts.layout')

@section('page-title')
     تعديل المناصب - {{$group->name}}
@endsection

@section('content')
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex mr-auto">
                <div class="breadcrumb">
                    <a href="{{route('admin.home')}}" class="breadcrumb-item">
                        <i class="icon-home2 mr-2"></i>
                        الرئيسية
                    </a>
                    <a href="{{route('admin.groups.index')}}" class="breadcrumb-item">المناصب</a>
                    <span class="breadcrumb-item active">@yield('page-title')</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- Content area -->
    <div class="content">
        <!-- Form horizontal -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h3 class="panel-title">تعديل المناصب
                    <span class="badge badge-info">{{$group->name}}</span>
                </h3>
            </div>

            <div class="panel-body">
                {!! Form::model($group,['route' => ['admin.groups.update',$group->id], 'method'=>'put', 'files' =>true ]) !!}
                    @include('admin.groups.form')
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /form horizontal -->
    </div>
    <!-- /content area -->
@endsection
