<div class="form-group row">
    <label for="name" class="col-form-label col-lg-2 font-weight-bold">الاسم</label>
    <div class="col-lg-10">
        {!! Form::text('name',null,['class' =>'form-control '.($errors->has('name') ? ' is-invalid' : null),'placeholder'=> 'الاسم' ]) !!}
        @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>

<div class="form-group ">
    <h6 class="font-weight-bold">الصلاحيات</h6>

    <div class="row mb-2">
        <div class="col-12">
            <input type="checkbox" id="select-all" class="mr-2">
            <label for="select-all">اختار الكل</label>
        </div>
    </div>
    <div class="row">
        @foreach(dbRoles() as $key=>$role)
            <div class="col-sm-3">
                {{ Form::checkbox('roles[]', $key, null, ['class' => 'name mr-1','id'=>'basic_checkbox_'.$key]) }}
                <label for="basic_checkbox_{{$key}}">{{$role}}</label>
            </div>
        @endforeach
    </div>
</div>

<div class="form-group row">
    <button type="submit" class="btn btn-primary btn-block">حفظ</button>
</div>
