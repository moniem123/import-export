@extends('admin.layouts.layout')
@section('page-title')
    المديرين
@endsection
@section('content')
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex mr-auto">
                <div class="breadcrumb">
                    <a href="{{route('admin.home')}}" class="breadcrumb-item">
                        <i class="icon-home2 mr-2"></i>
                       الرئيسية
                    </a>
                    <span class="breadcrumb-item active">@yield('page-title')</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none">
                    <i class="icon-more"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- Content area -->
    <div class="content">
        <!-- Basic initialization -->
        <div class="panel panel-flat">
            @include('admin.layouts.status')

                <div class="panel-body mb-2">
                    <a href="{{route('admin.managers.create')}}" class="btn btn-primary mr-3">
                        <i class="icon-add" style="margin-left: 10px;"></i>
                        اضافة مدير
                    </a>
                </div>

            <table class="table datatable-button-init-basic table-hover table-responsive display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>البريد الاليكترونى</th>
                        <th>رقم الهاتف</th>
                        <th>المنصب</th>
                        <th class="text-center">العمليات</th>

                    </tr>
                </thead>
                <tbody>
                @foreach($admins as $index => $admin)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$admin->name}}</td>
                        <td>{{$admin->email}}</td>
                        <td>{{$admin->phone ?? 'لا توجد صلاحية محدده'}}</td>
                        <td>
                            {{optional($admin->group)->name?? 'لا يوجد منصب محدد'}}
                        </td>

                        <td class="text-center">
                            <div class="btn-group text-center">

                                @if($admin->is_active == 0)
                                    <button disabled="disabled" class="btn btn-sm btn-warning disabled">غير مفعل</button>
                                @else
                                    <button disabled class="btn btn-sm btn-success">مفعل</button>
                                @endif

                                <div class=" text-center">
                                    <a href="{{route('admin.managers.edit',$admin->id)}}"
                                       class="btn btn-primary btn-sm ml-2 rounded-circle">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>

                                {!! Form::open(['route' => ['admin.managers.destroy',$admin->id],'method' => 'delete']) !!}
                                    <button class="btn btn-danger btn-sm ml-2 rounded-circle">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                {!! Form::close() !!}


                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /basic initialization -->
    </div>
    <!-- /content area -->

@endsection
