<div class="form-group row">
    <label for="name" class="col-form-label col-12 col-lg-2">الاسم</label>
    <div class="col-12 col-lg-4">
        {!! Form::text('name',null,['class' =>'form-control '.($errors->has('name') ? ' is-invalid' : null),'placeholder'=> 'الاسم' ]) !!}
        @error('name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

    <label for="email" class="col-form-label col-12 col-lg-2 text-lg-right"> البريد الاليكترونى</label>
    <div class="col-12 col-lg-4">
        {!! Form::email('email',null,['class' =>'form-control '.($errors->has('email') ? ' is-invalid' : null),'placeholder'=>'البريد الاليكترونى']) !!}
        @error('email')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="phone" class="col-form-label col-12 col-lg-2"> رقم الهاتف </label>
    <div class="col-12 col-lg-4">
        {!! Form::tel('phone',null,['class' =>'form-control '.($errors->has('phone') ? ' is-invalid' : null),'placeholder'=> 'رقم الهاتف' ]) !!}
        @error('phone')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="roles" class="col-form-label col-12 col-lg-2  text-lg-right"> المنصب </label>
    <div class="col-12 col-lg-4">
        {{--<select name="groups" class="form-control">--}}
        {{--<option disabled selected>{{__('Select Role')}}</option>--}}
        {{--@foreach($groups as $role)--}}
        {{--<option value="{{$role->id}}">{{$role->name}}</option>--}}
        {{--@endforeach--}}
        {{--</select>--}}
        {!! Form::select("group_id",groups(),isset($user)?getUserGroup($user):null,
        ['class'=>'form-control','data-show-subtext'=>'true','data-live-search'=>'true','id' => 'group_id','placeholder'=>'اختر المنصب '])!!}
    </div>
</div>

<div class="form-group row">
    <label for="password" class="col-form-label col-12 col-lg-2"> كلمة المرور </label>
    <div class="col-12 col-lg-4">
        {!! Form::password('password',['class' =>'form-control '.($errors->has('password') ? ' is-invalid' : null),'placeholder'=>' كلمة المرور ' ]) !!}
        @error('password')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>

    <label for="password_confirmation" class="col-form-label col-12 col-lg-2 text-lg-right">تاكيد كلمة المرور</label>
    <div class="col-12 col-lg-4">
        {!! Form::password('password_confirmation' ,['class' =>'form-control '.($errors->has('password_confirmation') ? ' is-invalid' : null),'placeholder'=> 'تاكيد كلمة المرور' ]) !!}
        @error('password_confirmation')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</div>





<div class="form-group row">
    <button type="submit" class="btn btn-primary btn-block">حفظ</button>
</div>
