@extends('admin.layouts.layout')

@section('page-title')
    {{$admin->name}}تعديل المدير -
@endsection

@section('content')
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex mr-auto">
                <div class="breadcrumb">
                    <a href="{{route('admin.home')}}" class="breadcrumb-item">
                        <i class="icon-home2 mr-2"></i>
                        الرئيسية
                    </a>
                    <a href="{{route('admin.managers.index')}}" class="breadcrumb-item">المديرين</a>
                    <span class="breadcrumb-item active">@yield('page-title')</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none">
                    <i class="icon-more"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- Content area -->
    <div class="content">
        <!-- Form horizontal -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h3 class="panel-title">
                    تعديل المدير
                    <span class="badge badge-info">{{$admin->name}}</span>
                </h3>
            </div>
            <hr>
            <div class="panel-body">
                {!! Form::model($admin,['route' => ['admin.managers.update',$admin->id],'method'=>'put','files' =>true]) !!}
                    @include('admin.admins.form')
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /form horizontal -->
    </div>
    <!-- /content area -->
@endsection
