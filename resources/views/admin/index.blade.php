@extends('admin.layouts.layout')
@section('page-title')
    {{__('Dashboard')}} | {{__('Main')}}
@endsection

@section('content')
    <router-view></router-view>
@endsection
