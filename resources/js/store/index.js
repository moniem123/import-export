export default {
    state:{
        users:{},
        imports:{},
        exports:{},

    },
    getters:{
        getUsers(state){
            return state.users;
        },
        getImports(state){
            return state.imports;
        },
        getExports(state){
            return state.exports;
        }
    },
    actions:{
        getAllUsers(context){
            axios.get('/api/users')
                .then(({data})=>{
                    context.commit('users',data.users)
                })
                .catch((data)=>{
                    // console.log(data);
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                })
        },
        usersPaginate(context,payload){
            axios.get('/api/users?page='+payload)
                .then(({data})=>{
                    context.commit('users',data.users)
                })
                .catch((data)=>{
                    console.log(data);
                })
        },
        getAllImports(context){
            axios.get('/api/imports')
                .then(({data})=>{
                    context.commit('imports',data.imports)
                })
                .catch((data)=>{
                    console.log(data);
                })
        },
        importsPaginate(context,payload){
            axios.get('/api/imports?page='+payload)
                .then(({data})=>{
                    context.commit('imports',data.imports)
                })
                .catch((data)=>{
                    console.log(data);
                })
        },
        getAllExports(context){
            axios.get('/api/exports')
                .then(({data})=>{
                    context.commit('exports',data.exports)
                })
                .catch((data)=>{
                    console.log(data);
                })
        },
        exportsPaginate(context,payload){
            axios.get('/api/exports?page='+payload)
                .then(({data})=>{
                    context.commit('exports',data.exports)
                })
                .catch((data)=>{
                    console.log(data);
                })
        },
    },
    mutations:{
        users(state,data){
            return state.users=data;
        },
        imports(state,data){
            return state.imports=data;
        },
        exports(state,data){
            return state.exports=data;
        },
    }
}
