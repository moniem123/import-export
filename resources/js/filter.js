import Vue from 'vue';
import moment from 'moment';

Vue.filter('attachments_url',function (data) {
   if(typeof(data)=== 'object'){
       if(/\b\.pdf/.test(data.url)){
           return `https://docs.google.com/viewerng/viewer?url=${data.url}&embedded=true`;
       }else{
           return data.url;
       }
   }
   else
       return data;
});

Vue.filter('dateTime',function(data){
    return moment(data).locale('ar-sa').format('L');
})



Vue.filter('pdfTest',function(data){
   if(/\b\.pdf/.test(data)){
       return `https://docs.google.com/viewerng/viewer?url=${data}&embedded=true`;
   }
   return data;
})
