
export const routes = [
    { path: '/home', component: require('./components/admin/Home.vue').default},

    { path: '/users', component: require('./components/admin/users/Index.vue').default},
    { path: '/users/create', component: require('./components/admin/users/Create.vue').default},
    { path: '/users/:user_id/edit', component: require('./components/admin/users/Edit.vue').default},

    { path: '/import', component: require('./components/admin/imports/Index.vue').default},
    { path: '/import/create', component: require('./components/admin/imports/Create.vue').default},
    { path: '/import/:process_id/edit', component: require('./components/admin/imports/Edit.vue').default},

    { path: '/export', component: require('./components/admin/exports/Index.vue').default},
    { path: '/export/create', component: require('./components/admin/exports/Create.vue').default},
    { path: '/export/:process_id/edit', component: require('./components/admin/exports/Edit.vue').default},
    { path: '/*', component: require('./components/NotFound.vue').default },
]

