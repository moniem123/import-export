<?php
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;


function uploadBase64Image($dir_name,$request=null,$attr_name=null,$file=null){
    $dir = public_path('/storage/'.$dir_name);
    if(!is_dir($dir))
        mkdir($dir, 0777, true);

    $name =  $dir_name.'/'.Str::random(10) . '.' . 'png';
    if($request)
        Image::make($request[$attr_name])->save( public_path('/storage/').$name);
    if($file){
        if(strpos($file, 'data:application/pdf;base64,') !== false){
            $file = str_replace('data:application/pdf;base64,', '', $file);
            $file = str_replace(' ', '+', $file);
            $name =  $dir_name.'/'.Str::random(10).'.'.'pdf';
            \File::put(public_path('/storage/').$name, base64_decode($file));
        }else{
            Image::make($file)->save( public_path('/storage/').$name);
        }
    }
    return $name;
}

function uploadBase64MultiImages($request,$attr_name,$dir_name,$model=null,$reference_id=null){
    $files = [];
    foreach ($request[$attr_name] as $key=>$file){
        $name =  uploadBase64Image($dir_name,null,null,$file);
        $files[$key] = $name;
        if($model){
            $model::create(['url'=>$name,'processes_id'=>$reference_id]);
        }
    }

    return $files;
}

function updateImageBase64($request,$attr_name,$dir_name,$model){
    if ($request->has($attr_name) && $model[$attr_name]!=$request[$attr_name]) {
        if($model[$attr_name] != null && !(strpos($model[$attr_name], 'https://lorempixel.com/') !== false)){
            $oldImage = public_path('/').(explode(url('/'),$model[$attr_name]))[1];
            if(file_exists($oldImage)){
                unlink($oldImage);
            }
        }
        return uploadBase64Image($dir_name,$request,$attr_name);
    }else{
        return explode('http://localhost:8000/storage/',$request[$attr_name])[1];
    }
}



function getImg($filename)
{
    if (!empty($filename)) {
        $base_url = url('/');
        return $base_url . '/storage/' . $filename;
    } else {
        return null;
    }
}



function uploadpath()
{
    return 'photos';
}

function uploadWithPath($folder_name)
{
    return 'photos/'.$folder_name;
}
function multiUploaderProduct($request,$img_name,$model,$onId=null){
    $model = new $model();
//    dd($onId,$model);
    foreach ($request[$img_name] as $image){
        $filename = rand(99999, 99999999) . $image->getClientOriginalName();
        $path = Storage::disk('public')->putFileAs(uploadWithPath($model->path), $image, $filename);
        $model->create(['url' => $path, 'processes_id' => $onId]);
    }
    return true;
}
