<?php

namespace App\Http\Controllers;

use App\Processes;
use App\User;
use Illuminate\Http\Request;

class HomeVueController extends Controller
{

    public function home(){
        $user = auth('api')->user();
        if($user->role == 'user'){
            $data['imports']= Processes::where('process_type','import')->where('user_id',$user->id)->count();
            $data['exports']= Processes::where('process_type','export')->where('user_id',$user->id)->count();
        }else{
            $data['users']= User::count();
            $data['imports']= Processes::where('process_type','import')->count();
            $data['exports']= Processes::where('process_type','export')->count();
        }

        return response()->json(['status'=>true,'data'=>$data],200);
    }
}
