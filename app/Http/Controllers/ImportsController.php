<?php

namespace App\Http\Controllers;

use App\Processes;
use App\ProcessesAttachments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class ImportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth('api')->user();
        if($user->role == 'user')
            $imports = Processes::where('process_type','import')->where('user_id',$user->id)->latest()->paginate(10);
        else
            $imports = Processes::where('process_type','import')->latest()->paginate(10);
        return response()->json(['status'=>true,'imports'=>$imports],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $pro = Processes::where('process_type','import')->latest()->first();
        if($pro){
            $num = $pro->process_num +1;
        }else{
            $num = 1;
        }
        Validator::make($request->all(),[
            "enterprise_name" => "required|string",
            "category_name" => "required|string",
//            "process_image" => "required|string",
            "attachments_url" => "required|array",
            "attachments_url.*" => "required|file",
//            'date'=>'required|date_format:Y-m-d',
            'year'=>'required|date_format:Y',
            'month'=>'required|date_format:m',
            'day'=>'required|date_format:d',
        ])->validate();

        $inputs = $request->except('bar_code','process_image','attachments_url','busy','successful','errors','originalData');
//        $inputs['process_image'] = uploadBase64Image('imports',$request,'process_image');
        $inputs['process_type'] = 'import';
        $inputs['bar_code'] = $num;
        $inputs['user_id'] = auth('api')->user()->id;
        $inputs['process_num'] =$num;
        $inputs['date'] = $request['year'].'-'.$request['month'].'-'.$request['day'];
        $import = Processes::create($inputs);
        multiUploaderProduct($request,'attachments_url','App\ProcessesAttachments',$import->id);
//        $attachments = uploadBase64MultiImages($request,'attachments_url','import_attachments','App\ProcessesAttachments',$import->id);
        return response()->json(['status'=>true, 'message'=>'تمت الاضافة بنجاح'],201);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth('api')->user();
        $import = Processes::where('process_type','import')->where('id',$id)->first();
        if($import->user_id =! $user->id || $user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        return response()->json(['status'=>true,'import'=>$import],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth('api')->user();
        $import = Processes::where('process_type','import')->where('id',$id)->first();
        if($import->user_id =! $user->id || $user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);

        Validator::make($request->all(),[
            "enterprise_name" => "required|string",
            "category_name" => "required|string",
            'year'=>'sometimes|date_format:Y',
            'month'=>'sometimes|date_format:m',
            'day'=>'sometimes|date_format:d',
        ])->validate();
        $inputs = $request->except('bar_code','process_image','attachments_url','busy','successful','errors','originalData');
//        $inputs['process_image'] = updateImageBase64($request,'process_image','imports',$import);
        $inputs['process_type'] = 'import';
        $inputs['bar_code'] = $import['process_num'];
        $inputs['user_id'] = $user->id;
        if($request['year']&&$request['month']&&$request['day'])
            $inputs['date'] =$request['year'].'-'.$request['month'].'-'.$request['day'];
        $import->update($inputs);
//        if(($request['attachments_url']!=[])&& (!is_array($request['attachments_url'][0]))){
            multiUploaderProduct($request,'attachments_url','App\ProcessesAttachments',$import->id);
//        }
        return response()->json(['status'=>true, 'message'=>'تم  التعديل بنجاح'],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth('api')->user();
        $precess = Processes::findOrFail($id);
        if($precess->user_id =! $user->id || $user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        if(!(strpos($precess['process_image'], 'https://lorempixel.com/') !== false)){
            $oldImage = public_path('/').(explode(url('/'),$precess['process_image']))[1];
            if(file_exists($oldImage)){
                unlink($oldImage);
            }
        }
        foreach ($precess->attachments as $attach){
            if(!(strpos($attach['url'], 'https://lorempixel.com/') !== false)){
                $oldAttach = public_path('/').(explode(url('/'),$attach['url']))[1];
                if(file_exists($oldAttach)){
                    unlink($oldAttach);
                }
            }
        }
        $precess->attachments()->delete();
        $precess->delete();
        return response()->json(['status'=>true, 'message'=>'تم الحذف بنجاح'],201);

    }

    public function destroyAttachment($id){
        $attachment = ProcessesAttachments::findOrFail($id);
        if(!(strpos($attachment['url'], 'https://lorempixel.com/') !== false)){
            $oldImage = public_path('/').(explode(url('/'),$attachment['url']))[1];
            if(file_exists($oldImage)){
                unlink($oldImage);
            }
        }

        $attachment->delete();
        return response()->json(['status'=>true, 'message'=>'تم الحذف بنجاح'],201);
    }
}
