<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth('api')->user();
        if($user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        $users = User::latest()->paginate(10);
        return response()->json(['status'=>true,'users'=>$users],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth('api')->user();
        if($user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        Validator::make($request->all(),[
            'name'=>'required|string',
            'email'=>'required|email|unique:users,email',
            'phone'=>'required|numeric|unique:users,phone',
            'password'=>'required|string|min:6|confirmed',
            'role'=>'required|string|in:admin,user',
        ])->validate();
        $inputs = $request->all();
        User::create($inputs);
        return response()->json(['status'=>true,'message'=>'تم الاضافة بنجاح'],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth('api')->user();
        if($user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        $user = User::findOrFail($id);
        return response()->json(['status'=>true,'user'=>$user],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth('api')->user();
        if($user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        $user = User::findOrFail($id);
        Validator::make($request->all(),[
            'name'=>'required|string',
            'email'=>'required|email|unique:users,email,'.$user->id,
            'phone'=>'required|numeric|unique:users,phone,'.$user->id,
            'role'=>'required|string|in:admin,user',
        ])->validate();
        if($request->has('password') && $request['password'] != null){
            Validator::make($request->all(),[
                "password" => "required|string|confirmed",
            ])->validated();
            $inputs = $request->except('image');
        }else{
            $inputs = $request->except('password','image');
        }
        $user->update($inputs);
        return response()->json(['status'=>true,'message'=>'تم التعديل بنجاح'],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth('api')->user();
        if($user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json(['status'=>true, 'message'=>'تم الحذف بنجاح'],201);
    }

    public function banToggle($id){
        $user = auth('api')->user();
        if($user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        $user = User::findOrFail($id);
        if($user->is_ban){
            $user->update(['is_ban'=>0]);
            return response()->json(['status'=>true,'message'=>'تم التفعيل'],200);
        }else{
            $user->update(['is_ban'=>1]);
            return response()->json(['status'=>true,'message'=>'تم الحظر'],200);
        }
    }
}
