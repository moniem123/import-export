<?php

namespace App\Http\Controllers;

use App\Processes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth('api')->user();
        if($user->role == 'user')
            $exports = Processes::where('process_type','export')->where('user_id',$user->id)->latest()->paginate(10);
        else
            $exports = Processes::where('process_type','export')->latest()->paginate(10);
        return response()->json(['status'=>true,'exports'=>$exports],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pro = Processes::where('process_type','export')->latest()->first();
        if($pro){
            $num = $pro->process_num +1;
        }else{
            $num = 1;
        }
        Validator::make($request->all(),[
            "category_name" => "required|string",
//            "process_image" => "required|string",
            "attachments_url" => "required|array",
            "attachments_url.*" => "required|file",
//            'date'=>'required|date_format:Y-m-d',
            'year'=>'required|date_format:Y',
            'month'=>'required|date_format:m',
            'day'=>'required|date_format:d',
        ])->validate();
        $inputs = $request->except('bar_code','process_image','attachments_url','busy','successful','errors','originalData');
//        $inputs['process_image'] = uploadBase64Image('exports',$request,'process_image');
        $inputs['process_type'] = 'export';
        $inputs['bar_code'] = $num;
        $inputs['user_id'] = auth('api')->user()->id;
        $inputs['process_num'] = $num;
        $inputs['enterprise_name'] = 'جمعية تجهيز';
        $inputs['date'] =$request['year'].'-'.$request['month'].'-'.$request['day'];
        $export = Processes::create($inputs);
        multiUploaderProduct($request,'attachments_url','App\ProcessesAttachments',$export->id);

//        $attachments = uploadBase64MultiImages($request,'attachments_url','export_attachments','App\ProcessesAttachments',$export->id);
        return response()->json(['status'=>true, 'message'=>'تمت الاضافة بنجاح'],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth('api')->user();
        $export = Processes::where('process_type','export')->where('id',$id)->first();
        if($export->user_id =! $user->id || $user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        return response()->json(['status'=>true,'export'=>$export],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth('api')->user();
        $export = Processes::where('process_type','export')->where('id',$id)->first();
        if($export->user_id =! $user->id || $user->role == 'user')
            return response()->json(['status'=>true, 'message'=>'ليس لديك صلاحية '],403);
        Validator::make($request->all(),[
            "category_name" => "required|string",
            'year'=>'sometimes|date_format:Y',
            'month'=>'sometimes|date_format:m|between:1,12',
            'day'=>'sometimes|date_format:d|between:1,30',
        ])->validate();
        $inputs = $request->except('bar_code','process_image','attachments_url','busy','successful','errors','originalData');
//        $inputs['process_image'] = updateImageBase64($request,'process_image','exports',$export);
        $inputs['process_type'] = 'export';
        $inputs['bar_code'] = $export->process_num;
        $inputs['user_id'] = $user->id;
        $inputs['enterprise_name'] = 'جمعية تجهيز';
        if($request['year']&&$request['month']&&$request['day'])
            $inputs['date'] =$request['year'].'-'.$request['month'].'-'.$request['day'];
        $export->update($inputs);
        if(($request['attachments_url']!=[])&& (!is_array($request['attachments_url'][0]))){
//            $attachments =uploadBase64MultiImages($request,'attachments_url','export_attachments','App\ProcessesAttachments',$export->id);
            multiUploaderProduct($request,'attachments_url','App\ProcessesAttachments',$export->id);

        }
        return response()->json(['status'=>true, 'message'=>'تم  التعديل بنجاح'],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth('api')->user();
        $precess = Processes::findOrFail($id);
        if ($precess->user_id = !$user->id || $user->role == 'user')
            return response()->json(['status' => true, 'message' => 'ليس لديك صلاحية '], 403);
        if(!(strpos($precess['process_image'], 'https://lorempixel.com/') !== false)){
        $oldImage = public_path('/') . (explode(url('/'), $precess['process_image']))[1];
            if (file_exists($oldImage)) {
                unlink($oldImage);
            }
        }
        foreach ($precess->attachments as $attach){
            if(!(strpos($attach['url'], 'https://lorempixel.com/') !== false)){
                $oldAttach = public_path('/').(explode(url('/'),$attach['url']))[1];
                if(file_exists($oldAttach)){
                    unlink($oldAttach);
                }
            }
        }
        $precess->attachments()->delete();
        $precess->delete();
        return response()->json(['status'=>true, 'message'=>'تم الحذف بنجاح'],201);
    }
}
