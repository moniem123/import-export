<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessesAttachments extends Model
{
    // type => [image,pdf]
    protected $fillable = ['url','type','processes_id'];

    public function process(){
        return $this->belongsTo(Processes::class,'processes_id');
    }

    public function getUrlAttribute(){
        if(strpos($this->attributes['url'], 'https://lorempixel.com/') !== false){
            return $this->attributes['url'];
        }elseif($this->attributes['url']){
            return getImg($this->attributes['url']);
        }
        return getImg('icon-user-default.png');
    }
}
