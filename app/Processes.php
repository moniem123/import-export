<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Processes extends Model
{

    // process_type => [import,export]
    protected $fillable = ['process_num','enterprise_name','category_name','bar_code','process_image','process_type','user_id','date'];
    protected $appends = ['attachment_count','attachments_url'];

    public function attachments(){
        return $this->hasMany(ProcessesAttachments::class,'processes_id');
    }

    public function getAttachmentCountAttribute()
    {
        return $this->attachments->count();
    }

    public function getAttachmentsUrlAttribute()
    {
        return $this->attachments->transform(function ($q){
            return[
                'id'=>$q->id,
                'url'=>$q->url,
            ];
        });
    }

    public function getProcessImageAttribute(){
        if(strpos($this->attributes['process_image'], 'https://lorempixel.com/') !== false){
            return $this->attributes['process_image'];
        }elseif($this->attributes['process_image']){
            return getImg($this->attributes['process_image']);
        }
        return getImg('icon-user-default.png');
    }

    public function getDateAttribute(){
        return date('Y/m/d',strtotime($this->attributes['date']));
    }

}
