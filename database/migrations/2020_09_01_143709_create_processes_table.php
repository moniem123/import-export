<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processes', function (Blueprint $table) {
            $table->id();
            $table->string('process_num')->nullable();
            $table->string('enterprise_name')->nullable();
            $table->string('category_name')->nullable();
            $table->string('bar_code')->nullable();
            $table->string('process_image')->nullable();
            $table->string('process_type')->nullable(); // [import,export]
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processes');
    }
}
