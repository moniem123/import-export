<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessesAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processes_attachments', function (Blueprint $table) {
            $table->id();

            $table->string('url')->nullable();
            $table->string('type')->nullable(); // [image,pdf]
            $table->foreignId('processes_id')->nullable()->constrained('processes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processes_attachments');
    }
}
