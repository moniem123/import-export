<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Processes;
use Faker\Generator as Faker;

$process_type = ['import','export'];
$factory->define(Processes::class, function (Faker $faker) use ($process_type) {
    return [
        'process_num'=>$faker->randomNumber(6),
        'enterprise_name'=>$faker->name,
        'category_name'=>$faker->name,
        'bar_code'=>$faker->ean13,
        'process_image'=>$faker->imageUrl(),
        'process_type'=>$process_type[array_rand($process_type)]
    ];
});
